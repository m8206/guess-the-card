import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MainWindow {

	private JPanel contentPane;
	JFrame frame;
	static MainWindow window;
	private int clicked = 0;
	private static int one = 0;
	private static int two = 0;
	private static int three = 0;
	static boolean check = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static Integer randomNumber(List<Integer> numberRandom) {
		Random rand = new Random();
		int number = rand.nextInt(3) + 1;

		if (numberRandom.isEmpty()) {
			numberRandom.add(number);
			if (number == 1) {
				one++;
			} else if (number == 2) {
				two++;
			} else {
				three++;
			}

		} else {
			if (number == 1) {
				if (one < 2) {
					numberRandom.add(number);
					one++;
				} else {
					while (check == false) {
						number = rand.nextInt(3) + 1;
						checkNum(number, numberRandom);
					}

					check = false;
					return number;

				}
			} else if (number == 2) {
				if (two < 2) {
					numberRandom.add(number);
					two++;
				} else {
					while (check == false) {
						number = rand.nextInt(3) + 1;
						checkNum(number, numberRandom);
					}

					check = false;
					return number;
				}
			} else {
				if (three < 2) {
					numberRandom.add(number);
					three++;
				} else {
					while (check == false) {
						number = rand.nextInt(3) + 1;
						checkNum(number, numberRandom);
					}
					check = false;
					return number;
				}
			}

		}

		return number;

	}

	public static void checkNum(int number, List<Integer> numberRandom) {

		if (number == 1) {
			if (one < 2) {
				one++;
				numberRandom.add(number);
				check = true;
			}

		} else if (number == 2) {
			if (two < 2) {
				two++;
				numberRandom.add(number);
				check = true;
			}
		} else {
			if (three < 2) {
				three++;
				numberRandom.add(number);
				check = true;
			}
		}

	}


	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Guess The Card");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel MyPanel = new JPanel();
		MyPanel.setLayout(new GridLayout(3, 2)); // 2x3 Grid

		List<Integer> number = new ArrayList<>();
		List<JButton> clickedButton = new ArrayList<>();

		JButton card1 = new JButton(".");
		JButton card2 = new JButton(".");
		JButton card3 = new JButton(".");
		JButton card4 = new JButton(".");
		JButton card5 = new JButton(".");
		JButton card6 = new JButton(".");

		card1.setActionCommand(Integer.toString(randomNumber(number)));
		card2.setActionCommand(Integer.toString(randomNumber(number)));
		card3.setActionCommand(Integer.toString(randomNumber(number)));
		card4.setActionCommand(Integer.toString(randomNumber(number)));
		card5.setActionCommand(Integer.toString(randomNumber(number)));
		card6.setActionCommand(Integer.toString(randomNumber(number)));

		MyPanel.add(card1);
		MyPanel.add(card2);
		MyPanel.add(card3);
		MyPanel.add(card4);
		MyPanel.add(card5);
		MyPanel.add(card6);

		frame.getContentPane().add(MyPanel, "Center"); // Paste MyPanel into JFrame

		frame.setSize(521, 300);

		card1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				card1.setText(card1.getActionCommand());
				card1.setEnabled(false);
				clicked++;

				if (!clickedButton.isEmpty()) {
					if (!clickedButton.get(0).getActionCommand().equals(card1.getActionCommand())) {
						JOptionPane.showMessageDialog(null, "Not Match!!");
						window.frame.dispose();
						one = 0;
						two = 0;
						three = 0;
						window = new MainWindow();
						window.frame.setVisible(true);
					}

				} else {
					clickedButton.add(0, card1);
				}

				if (clicked == 6) {
					JOptionPane.showMessageDialog(null, "You Win !!");
					window.frame.setVisible(false);
					one = 0;
					two = 0;
					three = 0;
					window = new MainWindow();
					window.frame.setVisible(true);
				} else if (clicked % 2 == 0) {
					clickedButton.remove(0);
				} else {

				}
			}
		});

		card2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				card2.setText(card2.getActionCommand());
				card2.setEnabled(false);
				clicked++;

				if (!clickedButton.isEmpty()) {
					if (!clickedButton.get(0).getActionCommand().equals(card2.getActionCommand())) {
						JOptionPane.showMessageDialog(null, "Not Match!!");
						window.frame.dispose();
						one = 0;
						two = 0;
						three = 0;
						window = new MainWindow();
						window.frame.setVisible(true);
					}

				} else {
					clickedButton.add(0, card2);
				}

				if (clicked == 6) {
					JOptionPane.showMessageDialog(null, "You Win !!");
					window.frame.setVisible(false);
					one = 0;
					two = 0;
					three = 0;
					window = new MainWindow();
					window.frame.setVisible(true);
				} else if (clicked % 2 == 0) {
					clickedButton.remove(0);
				} else {

				}

			}
		});

		card3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				card3.setText(card3.getActionCommand());
				card3.setEnabled(false);
				clicked++;

				if (!clickedButton.isEmpty()) {
					if (!clickedButton.get(0).getActionCommand().equals(card3.getActionCommand())) {
						JOptionPane.showMessageDialog(null, "Not Match!!");
						window.frame.dispose();
						one = 0;
						two = 0;
						three = 0;
						window = new MainWindow();
						window.frame.setVisible(true);
					}

				} else {
					clickedButton.add(0, card3);
				}

				if (clicked == 6) {
					JOptionPane.showMessageDialog(null, "You Win !!");
					window.frame.setVisible(false);
					one = 0;
					two = 0;
					three = 0;
					window = new MainWindow();
					window.frame.setVisible(true);
				} else if (clicked % 2 == 0) {
					clickedButton.remove(0);
				} else {

				}

			}
		});

		card4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				card4.setText(card4.getActionCommand());
				card4.setEnabled(false);
				clicked++;

				if (!clickedButton.isEmpty()) {
					if (!clickedButton.get(0).getActionCommand().equals(card4.getActionCommand())) {
						JOptionPane.showMessageDialog(null, "Not Match!!");
						window.frame.dispose();
						one = 0;
						two = 0;
						three = 0;
						window = new MainWindow();
						window.frame.setVisible(true);
					}

				} else {
					clickedButton.add(0, card4);
				}

				if (clicked == 6) {
					JOptionPane.showMessageDialog(null, "You Win !!");
					window.frame.setVisible(false);
					one = 0;
					two = 0;
					three = 0;
					window = new MainWindow();
					window.frame.setVisible(true);
				} else if (clicked % 2 == 0) {
					clickedButton.remove(0);
				} else {

				}
			}

		});

		card5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				card5.setText(card5.getActionCommand());
				card5.setEnabled(false);
				clicked++;

				if (!clickedButton.isEmpty()) {
					if (!clickedButton.get(0).getActionCommand().equals(card5.getActionCommand())) {
						JOptionPane.showMessageDialog(null, "Not Match!!");
						window.frame.dispose();
						one = 0;
						two = 0;
						three = 0;
						window = new MainWindow();
						window.frame.setVisible(true);
					}

				} else {
					clickedButton.add(0, card5);
				}

				if (clicked == 6) {
					JOptionPane.showMessageDialog(null, "You Win !!");
					window.frame.setVisible(false);
					one = 0;
					two = 0;
					three = 0;
					window = new MainWindow();
					window.frame.setVisible(true);
				} else if (clicked % 2 == 0) {
					clickedButton.remove(0);
				} else {

				}
			}

		});

		card6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				card6.setText(card6.getActionCommand());
				card6.setEnabled(false);
				clicked++;

				if (!clickedButton.isEmpty()) {
					if (!clickedButton.get(0).getActionCommand().equals(card6.getActionCommand())) {
						JOptionPane.showMessageDialog(null, "Not Match!!");
						window.frame.dispose();
						one = 0;
						two = 0;
						three = 0;
						window = new MainWindow();
						window.frame.setVisible(true);
					}

				} else {
					clickedButton.add(0, card6);
				}

				if (clicked == 6) {
					JOptionPane.showMessageDialog(null, "You Win !!");
					window.frame.setVisible(false);
					one = 0;
					two = 0;
					three = 0;
					window = new MainWindow();
					window.frame.setVisible(true);
				} else if (clicked % 2 == 0) {
					clickedButton.remove(0);
				} else {

				}
			}

		});

	}

}
